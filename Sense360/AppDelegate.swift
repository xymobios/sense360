//
//  AppDelegate.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import SenseSdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, ServerWrapperDelegate, RecipeFiredDelegate
{
    
    var logClearCount = Int()
    
    var window: UIWindow?
    let manager  = CLLocationManager()
    let backend = ServerWrapper()
    let constants = Constants()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        Helper.printLog("Inside didFinishLaunchingWithOptions")
        
        SenseSdk.enableSdkWithKey(constants.APP_KEY)
        
        logClearCount = 0
        application.statusBarStyle = UIStatusBarStyle.LightContent
        
        // register recipe
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if Helper.isUserRegistred()
        {
            registerRecipeWithSenseSDK()
        }

        manager.delegate = self
        setRootController()
        self.window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication)
    {
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
        // Clear logs if user has opened the app for more than 20 times
        logClearCount = logClearCount + 1
        
        if logClearCount == 20
        {
            logClearCount = 0
            
            if constants.LOG_ENABLE == 1
            {
                ClearLog()
            }
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if Helper.isUserRegistred()
        {
            // If location Permission is allowed, show "ConfirmationScreen", else show "LocationPermission" Screen
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways
            {
                let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! LocationPermissionScreen
                self.window?.rootViewController = locationPermissionController
                locationPermissionController.viewDidAppear(false)
            }
            else
            {
                let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
                self.window?.rootViewController = confirmationScreen
                confirmationScreen.viewDidAppear(false)
            }
        }
        else
        {
            let registrationScreen = storyboard.instantiateViewControllerWithIdentifier("RegistrationController") as! UIViewController
            self.window?.rootViewController = registrationScreen
        }
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
    }

    func applicationWillTerminate(application: UIApplication)
    {
    }

    // MARK - CLLocation MAnager Delegate Methods
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status == CLAuthorizationStatus.AuthorizedAlways
        {
            NSLog("CLAuthorizationStatus > location is authorized")
            
            let defaults = NSUserDefaults.standardUserDefaults()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if Helper.isUserRegistred()
            {
                // Show Confirmation Screen, after user allows for Location Permission
                let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
                self.window?.rootViewController = confirmationScreen
                confirmationScreen.viewDidAppear(false)
            }
            else
            {
                let registrationScreen = storyboard.instantiateViewControllerWithIdentifier("RegistrationController") as! UIViewController
                self.window?.rootViewController = registrationScreen
            }
            
            // Register Recipe if Email Registration Done
            if Helper.isUserRegistred() &&
            !defaults.boolForKey(constants.kRecipeRegistrationDone)
            {
                defaults.setBool(true, forKey: constants.kRecipeRegistrationDone)
                defaults.synchronize()
                registerRecipeWithSenseSDK()
            }
        }
        else if status == CLAuthorizationStatus.AuthorizedWhenInUse
        {
            NSLog("CLAuthorizationStatus > AuthorizedWhenInUse")
        }
        else if status == CLAuthorizationStatus.NotDetermined
        {
            NSLog("CLAuthorizationStatus > NotDetermined")
        }
        else if status == CLAuthorizationStatus.Denied
        {
            NSLog("CLAuthorizationStatus > Denied")
        }
    }
    
    // MARK - User Defined Functions
    
    func setRootController()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let defaults = NSUserDefaults.standardUserDefaults()
        
        // If Email Registeration is not done, show Registration Screen, else show next screen in flow
        if Helper.isUserRegistred()
        {
            // If location permission is not allowed show "ConfirmationScreen", else show "LocationPermission" Screen
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways
            {
                let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! UIViewController
                self.window?.rootViewController = locationPermissionController
            }
            else
            {
                let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
                self.window?.rootViewController = confirmationScreen
                confirmationScreen.viewDidAppear(false)
            }
        }
        else
        {
            let registrationScreen = storyboard.instantiateViewControllerWithIdentifier("RegistrationController") as! UIViewController
            self.window?.rootViewController = registrationScreen
        }
    }
    
    // MARK - SenseAPI Related
    
    func registerRecipeWithSenseSDK()
    {
        Helper.printLog("Inside registerRecipeWithSenseSDK")
        
        let errorPointer = SenseSdkErrorPointer.create()
        
        /********************************* Register POI Recipes *********************************/
        
        let airportEnterTrigger: Trigger? = FireTrigger.whenEntersPoi(.Airport, errorPtr: errorPointer)
        if let triggerAirportEnter = airportEnterTrigger
        {
            registerRecipe(constants.airportEnterRecipe, trigger: triggerAirportEnter)
        }
        else
        {
            Helper.printLog("Error building trigger Airport Enter Trigger. Msg=" + errorPointer.error.message)
        }
        
        let airportExitTrigger: Trigger? = FireTrigger.whenExitsPoi(.Airport, errorPtr: errorPointer)
        if let triggerAirportExit = airportExitTrigger
        {
            registerRecipe(constants.airportExitRecipe, trigger: triggerAirportExit)
        }
        else
        {
            Helper.printLog("Error building trigger Airport Exit Trigger. Msg=" + errorPointer.error.message)
        }
        
        let barEnterTrigger: Trigger? = FireTrigger.whenEntersPoi(.Bar, errorPtr: errorPointer)
        if let triggerBarEnter = barEnterTrigger
        {
            registerRecipe(constants.barEnterRecipe, trigger: triggerBarEnter)
        }
        else
        {
            Helper.printLog("Error building trigger Bar Enter Trigger. Msg=" + errorPointer.error.message)
        }
        
        let barExitTrigger: Trigger? = FireTrigger.whenExitsPoi(.Bar, errorPtr: errorPointer)
        if let triggerBarExit = barExitTrigger
        {
            registerRecipe(constants.barExitRecipe, trigger: triggerBarExit)
        }
        else
        {
            Helper.printLog("Error building trigger Bar Exit Trigger. Msg=" + errorPointer.error.message)
        }
        
        let restaurantEnterTrigger: Trigger? = FireTrigger.whenEntersPoi(.Restaurant, errorPtr: errorPointer)
        if let triggerRestaurantEnter = restaurantEnterTrigger
        {
            registerRecipe(constants.restaurantEnterRecipe, trigger: triggerRestaurantEnter)
        }
        else
        {
            Helper.printLog("Error building trigger Restaurant Enter Trigger. Msg=" + errorPointer.error.message)
        }
        
        let restaurantExitTrigger: Trigger? = FireTrigger.whenExitsPoi(.Restaurant, errorPtr: errorPointer)
        if let triggerRestaurantExit = restaurantExitTrigger
        {
            registerRecipe(constants.restaurantExitRecipe, trigger: triggerRestaurantExit)
        }
        else
        {
            Helper.printLog("Error building trigger Restaurant Exit Trigger. Msg=" + errorPointer.error.message)
        }
        
        let mallEnterTrigger: Trigger? = FireTrigger.whenEntersPoi(.Mall, errorPtr: errorPointer)
        if let triggerMallEnter = mallEnterTrigger
        {
            registerRecipe(constants.mallEnterRecipe, trigger: triggerMallEnter)
        }
        else
        {
            Helper.printLog("Error building trigger Mall Enter Trigger. Msg=" + errorPointer.error.message)
        }
        
        let mallExitTrigger: Trigger? = FireTrigger.whenExitsPoi(.Mall, errorPtr: errorPointer)
        if let triggerMallExit = mallExitTrigger
        {
            registerRecipe(constants.mallExitRecipe, trigger: triggerMallExit)
        }
        else
        {
            Helper.printLog("Error building trigger Mall Exit Trigger. Msg=" + errorPointer.error.message)
        }

        let cafeEnterTrigger: Trigger? = FireTrigger.whenEntersPoi(.Cafe, errorPtr: errorPointer)
        
        if let triggerCafeEnter = cafeEnterTrigger
        {
            registerRecipe(constants.cafeEnterRecipe, trigger: triggerCafeEnter)
        }
        else
        {
            Helper.printLog("Error building trigger Cafe Enter Trigger. Msg=" + errorPointer.error.message)
        }
        
        let cafeExitTrigger: Trigger? = FireTrigger.whenExitsPoi(.Cafe, errorPtr: errorPointer)
        if let triggerCafeExit = cafeExitTrigger
        {
            registerRecipe(constants.cafeExitRecipe, trigger: triggerCafeExit)
        }
        else
        {
            Helper.printLog("Error building trigger Cafe Exit Trigger. Msg=" + errorPointer.error.message)
        }
        
        let gymEnterTrigger: Trigger? = FireTrigger.whenEntersPoi(.Gym, errorPtr: errorPointer)
        
        if let triggerGymEnter = gymEnterTrigger
        {
            registerRecipe(constants.gymEnterRecipe, trigger: triggerGymEnter)
        }
        else
        {
            Helper.printLog("Error building trigger Gym Enter Trigger. Msg=" + errorPointer.error.message)
        }
        
        let gymExitTrigger: Trigger? = FireTrigger.whenExitsPoi(.Gym, errorPtr: errorPointer)
        if let triggerGymExit = gymExitTrigger
        {
            registerRecipe(constants.gymExitRecipe, trigger: triggerGymExit)
        }
        else
        {
            Helper.printLog("Error building trigger Gym Exit Trigger. Msg=" + errorPointer.error.message)
        }
        
        /******************************* Register Personalized Recipes ***************************/
        
        let workEnterTrigger = FireTrigger.whenEntersPersonalizedPlace(.Work, errorPtr: errorPointer)
        if let triggerWorkEnter = workEnterTrigger
        {
            registerRecipe(constants.workEnterRecipe, trigger: triggerWorkEnter)
        }
        else
        {
            Helper.printLog("Error building trigger Work Enter Trigger. Msg=" + errorPointer.error.message)
        }
        
        let homeEnterTrigger = FireTrigger.whenEntersPersonalizedPlace(.Home, errorPtr: errorPointer)
        if let triggerHomeEnter = homeEnterTrigger
        {
            registerRecipe(constants.homeEnterRecipe, trigger: triggerHomeEnter)
        }
        else
        {
            Helper.printLog("Error building trigger Home Enter Trigger. Msg=" + errorPointer.error.message)
        }
        
        // define Personalized place Exit Triggers
        let workExitTrigger = FireTrigger.whenExitsPersonalizedPlace(.Work, errorPtr: errorPointer)
        if let triggerWorkExit = workExitTrigger
        {
            registerRecipe(constants.workExitRecipe, trigger: triggerWorkExit)
        }
        else
        {
            Helper.printLog("Error building trigger Work Exit Trigger. Msg=" + errorPointer.error.message)
        }
        
        let homeExitTriggers = FireTrigger.whenExitsPersonalizedPlace(.Home, errorPtr: errorPointer)
        if let triggerHomeExit = homeExitTriggers
        {
            registerRecipe(constants.homeExitRecipe, trigger: triggerHomeExit)
        }
        else
        {
            Helper.printLog("Error building trigger Home Exit Trigger. Msg=" + errorPointer.error.message)
        }
    }
    
    // custom method to register Recipe
    
    func registerRecipe(name: NSString, trigger: Trigger)
    {
        let errorPointer = SenseSdkErrorPointer.create()
        let recipe = Recipe(name: name as String, trigger: trigger, timeWindow: TimeWindow.allDay)
        
        let success = SenseSdk.register(recipe: recipe, delegate: self)
        
        if success
        {
            Helper.printLog("Recipe \(name) registered successfully")
        }
        else
        {
            Helper.printLog("Recipe \(name) failed to register")
        }
    }
    
    // MARK - RecipeFiredDelegate Methods
    func recipeFired(args: RecipeFiredArgs)
    {
        Helper.printLog("Inside recipeFired")
        
        var miliseconds = Int64(args.timestamp.timeIntervalSince1970 * 1000)
        
        if args.triggersFired.count > 0
        {
            let triggerFired = args.triggersFired[0]
            if triggerFired.places.count > 0
            {
                let place = triggerFired.places[0]
                let transitionDesc = args.recipe.trigger.transitionType.description
                
                switch(place.type)
                {
                case .CustomGeofence:
                    if let geofence = place as? CustomGeofence
                    {
                        Helper.GetAppDelegate().backend.sendTriggerInfo(constants.TRIGGER_PLACE_TYPE_CUSTOM, place: geofence.customIdentifier, action: transitionDesc, timeStamp: miliseconds, delegate: self)
                    }
                    break;
                case .Personal:
                    if let personal = place as? PersonalizedPlace
                    {
                        Helper.printLog("Personalized Place type trigger Fired")
                        Helper.GetAppDelegate().backend.sendTriggerInfo(constants.TRIGGER_PLACE_TYPE_PERSONALIZED, place: personal.personalizedPlaceType.description, action: transitionDesc, timeStamp: miliseconds, delegate: self)
                    }
                    break;
                case .Poi:
                    if let poi = place as? PoiPlace
                    {
                        Helper.printLog("POI Place type trigger Fired")
                        Helper.GetAppDelegate().backend.sendTriggerInfo(constants.TRIGGER_PLACE_TYPE_POI, place: poi.types[0].description, action: transitionDesc, timeStamp: miliseconds, delegate: self)
                    }
                    break;
                }
            }
        }
    }
    
    // MARK - Server Wrapper Delegate Methods
    
    func triggerInfoSentSuccessfully(results: NSDictionary)
    {
        Helper.printLog("Trigger Info Sent Successfully")
    }
    
    func requestFailed(requestId: Int, errorInfo: NSDictionary)
    {
        Helper.printLog("requestFailed with Error \(errorInfo)")
    }
}

