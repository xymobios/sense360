//
//  Constants.swift
//  TrackMyLocation
//
//  Created by Nidhi Sharma on 6/4/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

public struct Constants
{
    // App key
    let APP_KEY                             =   "sensequantified06072015"
    
    // App name
    let APP_NAME                            =   "Sense360"
    
    // to enable logging set this value as "1"
    let LOG_ENABLE                          =   0

    //************************  Recipe related ******************************/
    
    // Enter POI recipe
    let airportEnterRecipe                  =   "airportEnterRecipe"
    let barEnterRecipe                      =   "barEnterRecipe"
    let restaurantEnterRecipe               =   "restaurantEnterRecipe"
    let mallEnterRecipe                     =   "mallEnterRecipe"
    let cafeEnterRecipe                     =   "cafeEnterRecipe"
    let gymEnterRecipe                      =   "gymEnterRecipe"
    
    // Exit POI Recipe
    let airportExitRecipe                   =   "airportExitRecipe"
    let barExitRecipe                       =   "barExitRecipe"
    let restaurantExitRecipe                =   "restaurantExitRecipe"
    let mallExitRecipe                      =   "mallExitRecipe"
    let cafeExitRecipe                      =   "cafeExitRecipe"
    let gymExitRecipe                       =   "gymExitRecipe"

    // Enter Personalized place Recipe
    let workEnterRecipe                     =   "workEnterRecipe"
    let homeEnterRecipe                     =   "homeEnterRecipe"
    
    // Exit Personalized Place Recipe
    let workExitRecipe                      =   "workExitRecipe"
    let homeExitRecipe                      =   "homeExitRecipe"
    
    // Urls
    let URL_CREATE_ACCOUNT                  =  "http://www.xymob.com/s360/account/create"
    let URL_SEND_TRIGGER_INFO               =  "http://www.xymob.com/s360/trigger/message"
    let URL_TERMS_OF_USE                    =  "http://www.sense360.com/quantifiedplaceprivacy"
    let URL_VIEW_REPORT                     =  "http://www.xymob.com/s360/qp/wrsummary"

    
    // Application keys
    let kToken                              =   "token"
    let kSession                            =   "session"
    let kPlatform                           =   "platform"
    let kDeviceId                           =   "device"
    let kAppVersion                         =   "app-version"
    let kVersion                            =   "version"
    let kAppCode                            =   "app-code"
    
    let kMessage                            =   "message"
    let kCode                               =   "code"
    let kError                              =   "Error"
    let kWarning                            =   "Warning"
    
    let kUserInfo                           =   "userInfo"
    let kUserPhone                          =   "userPhone"
    let ksmsText                            =   "smsText"
    let kStartTime                          =   "startTime"
    let kEndTime                            =   "endTime"
    let kTimeFormat                         =   "hh a"
    
    // Server Keys
    let kServerError                        =   "error"
    let kServerEmail                        =   "email"
    let kServerMsg                          =   "msg"
    let kServerStatus                       =   "status"
    
    // Constants
    let kRecipeRegistrationDone             =   "RecipeRegistrationDone"
    let kUserEmail                          =   "userEmail"
    
    // Button Names
    let kCancel                             =   "Cancel"
    let kOpenSettings                       =   "Open Settings"
    let kOk                                 =   "Ok"
    let kRestart                            =   "Re-start"
    let kPause                              =   "Pause"
    
    
    // Request Time Interval
    let REQUEST_TIME_OUT : NSTimeInterval   =   60
    
    // Http Method Types
    let HTTP_METHOD_GET                     =   "GET"
    let HTTP_METHOD_POST                    =   "POST"
    let HTTP_METHOD_PUT                     =   "PUT"
    let HTTP_METHOD_HEAD                    =   "HEAD"
    let HTTP_METHOD_DELETE                  =   "DELETE"
    let HTTP_METHOD_PATCH                   =   "PATCH"
    
    // Request Ids
    let REQUEST_ID_CREATE_ACCOUNT           =   1
    let REQUEST_ID_SEND_TRIGGER_INFO        =   2
    
    // Trigger place type
    let TRIGGER_PLACE_TYPE_POI              =   1
    let TRIGGER_PLACE_TYPE_PERSONALIZED     =   2
    let TRIGGER_PLACE_TYPE_CUSTOM           =   3
    
    // Alert Message Strings
    let kNetworkFailed                      =   "Network Failed, please try again later."
    let KEnterName                          =   "Please enter your name."
    let KEnterEmail                         =   "Please enter your email Id."
    let kEnterValidEmail                    =   "Please enter valid email Id."
    let kEnterPassword                      =   "Please enter password."
    let kAcceptTermsOfUse                   =   "Please accept Terms of Use and Privacy Policy."
    let kErrorMessage                       =   "Some error occured, please try again later."
    
    // SetPhoneScreen Related
    let kUserTextMessage                    =   "Hey - just wanted to let you know I just left the office."
    let kEnterPhone                         =   "Please enter phone number on which text should be sent."
    let kEnterSMSText                       =   "Please enter SMS text."
    let kSetStartTime                       =   "Set start time"
    let kSetEndTime                         =   "Set end time"
    
    // Location Permission Screen Related
    let kBGAccessDisbaledTitle              =   "Background Location Access Disabled"
    let kLocationDeniedMsg                  =   "In order to provide you with place analytics, we need to access your location."
    
    let kNoInternetConnectionMsg            =   "We are unable to connect to the network. Please make sure that your device is connected to the internet."
}