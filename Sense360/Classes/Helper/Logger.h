//
//  Logger.h
//

#import <Foundation/Foundation.h>

/********************************************************************************!
 *
 *  @brief This class is very usefull for logging purpose. User can log the
 *  the text information in the file and read back from it.
 *  
 *  Step to adding log:
 
    Include Logger.h in desired file. Then add following code:
 
    --------------
    Plain text
    --------------
 
    #ifdef LOG_ENABLE
        LOG(@"I will be logged now...");
    #endif

    --------------
    Formatted text
    --------------
 
    #ifdef LOG_ENABLE
        LOG([@"<b><font color='green'>Current Tab: " stringByAppendingFormat:@"%d</font></b>",tabIndex]);
    #endif
 
 *
 *  Important Note: This file is written in C-style
 ********************************************************************************/

#define LOG_ENABLE

#ifdef LOG_ENABLE
#define LOG Log
#else
#define LOG
#endif

#ifdef LOG_ENABLE
#define W2FILE(a, b)	fwrite(&a, sizeof(a), 1, b)
#define RFILE(a, b)	fread(&a, sizeof(a), 1, b)

#if defined __cplusplus
extern "C" {
#endif
    
/*!
 *  @brief Log the textual information
 *  This method internally call to the WSTR2FILE fucntion for writting the contents
 */
void Log(NSString *str);

/*!
 *  @brief write content in the file
 */
void WSTR2FILE(NSString *str, FILE *file);
    
    
/*!
*  @brief clear the log, basically it removes the file
*/
void ClearLog();

#if defined __cplusplus
};
#endif

#endif


@interface Logger : NSObject
{

}

@end
