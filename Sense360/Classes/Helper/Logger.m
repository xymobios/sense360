//
//  Logger.m
//

#import "Logger.h"

#ifdef LOG_ENABLE
void WSTR2FILE(NSString *str, FILE *file)
{
	char buf[500];
	if(str)
	{
		[str getCString:buf maxLength:500 encoding: NSUTF8StringEncoding];
		int n = 0;
		while(buf[n])
		{
			n++;
		}
		fwrite(&buf, sizeof(char), n, file);
	}
}

void Log(NSString *str)
{
	NSDate *date = [NSDate date];
	NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[NSDateFormatter setDefaultFormatterBehavior: NSDateFormatterBehavior10_4];
	
	NSLocale *usLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
	[dateFormatter setLocale:usLocale];
	
	[dateFormatter setDateFormat: @"EEE, dd MMM yyyy HH:mm:ss"];
	
	NSString *strWithDate = [NSString stringWithFormat: @"%@ %@", [dateFormatter stringFromDate: date], str];
	
	//NSLog(@"%@", str);
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableStatePath = [documentsDirectory stringByAppendingPathComponent:@"QPLog.txt"];
	
	FILE * fp = fopen([writableStatePath cStringUsingEncoding: NSASCIIStringEncoding], "ab");
	WSTR2FILE([NSString stringWithFormat:@"%@\n", strWithDate], fp);
	fclose(fp);
}

#endif

void ClearLog()
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableStatePath = [documentsDirectory stringByAppendingPathComponent:@"QPLog.txt"];
	[[NSFileManager defaultManager] removeItemAtPath: writableStatePath error: nil];
}

@implementation Logger

@end
