//
//  CustomWebView.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation
import UIKit

class CustomWebView: UIViewController, UIWebViewDelegate
{
    var constants = Constants()
     var urlString = NSString()
    
    // Outlets
    @IBOutlet var webView: UIWebView!
    @IBOutlet var back: UIButton!
    @IBOutlet var reload: UIButton!
    @IBOutlet var forward: UIButton!
    @IBOutlet var stop: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // MARK - ViewLifeCycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadWebView()
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(true)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    // MARK - User Defined Functions
    
    func loadWebView()
    {
        let url  = NSURL (string: urlString as String)
        var request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = constants.HTTP_METHOD_GET
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.timeoutInterval = Constants().REQUEST_TIME_OUT
        
        for (key,val) in Helper.GetAppDelegate().backend.requestHeaders
        {
            request.setValue(val as? String, forHTTPHeaderField: key as! String)
        }
        webView.loadRequest(request)
    }
    
    // MARK - WebViewDelegate Methods

    func webViewDidStartLoad(webView: UIWebView)
    {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        stop.hidden = false;
        reload.hidden = true;
        activityIndicator.hidden = false
        updateWebViewButtons()
    }
    
    func webViewDidFinishLoad(webView: UIWebView)
    {
        stop.hidden = true;
        reload.hidden = false;
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activityIndicator.hidden = true
        updateWebViewButtons()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError)
    {
        stop.hidden = true;
        reload.hidden = false;
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activityIndicator.hidden = true
        updateWebViewButtons()
    }
    
    func updateWebViewButtons()
    {
        forward.enabled = webView.canGoForward
        back.enabled = webView.canGoBack
        stop.enabled = webView.loading
        reload.enabled = !webView.loading
    }
    
    // MARK - IBActions
    
    @IBAction func closeButtonPressed(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func refreshPressed(sender: AnyObject)
    {
        webView.reload()
        updateWebViewButtons()
    }
    
    @IBAction func stopPressed(sender: AnyObject)
    {
        webView.stopLoading()
        updateWebViewButtons()
    }
}