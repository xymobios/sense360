//
//  ViewController.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIAlertViewDelegate, NSURLConnectionDelegate, ServerWrapperDelegate, UITextFieldDelegate
{
    var constants = Constants()
    
    // Outlets
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var progressIndicatingView: UIView!
    @IBOutlet var privacyAcceptButton: UIButton!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var container: UIScrollView!
    @IBOutlet var submitButton: UIButton!
    
    // MARK - ViewLifeCycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        emailField.autocorrectionType = UITextAutocorrectionType.No;
        passwordField.autocorrectionType = UITextAutocorrectionType.No;
        nameField.autocorrectionType = UITextAutocorrectionType.No;
        privacyAcceptButton.selected = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
        container.contentSize = CGSizeMake(self.view.frame.size.width,
                submitButton.frame.size.height +
                50 + submitButton.frame.origin.y);
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK - IBActions
    
    @IBAction func termsOfServicePressed(sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let webViewController = storyboard.instantiateViewControllerWithIdentifier("webViewIdentifier") as! CustomWebView
        webViewController.urlString = constants.URL_TERMS_OF_USE
        self.presentViewController(webViewController, animated: true, completion: nil)
    }
    
    @IBAction func dismissKeyboardPressed(sender: AnyObject)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func SignUpPressed(sender: AnyObject)
    {
        nameField.text = nameField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        emailField.text = emailField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        passwordField.text = passwordField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        var alertMessage : NSString = "";
        
        if nameField.text.isEmpty
        {
            alertMessage = constants.KEnterName
        }
        else if emailField.text.isEmpty
        {
            alertMessage = constants.KEnterEmail
        }
        else if !Helper.isValidEmail(emailField.text)
        {
            alertMessage = constants.kEnterValidEmail
        }
        else if passwordField.text.isEmpty
        {
            alertMessage = constants.kEnterPassword
        }
        else if privacyAcceptButton.selected == false
        {
            alertMessage = constants.kAcceptTermsOfUse
        }

        if alertMessage.length > 0
        {
            Helper.showAlertDialog(constants.APP_NAME, alertMessage: alertMessage)
            return
        }
        
        if Reachability.reachabilityForInternetConnection().isReachable() == false
        {
            Helper.showInternetConnectionWarning()
            return
        }
        
        // send request to server to create account
        Helper.GetAppDelegate().backend.createAccount(emailField.text, password: passwordField.text, delegate: self)
        progressIndicatingView.hidden = false
        self.view.endEditing(true)
    }
    
    // MARK - ServerWrapper Delegate Methods
    
    func registrationCompleted(results: NSDictionary)
    {
        NSLog("registration completed \(results)")
        
        progressIndicatingView.hidden = true
        
        if results.objectForKey(constants.kToken) != nil &&
        results.objectForKey(constants.kToken)?.length != 0
        {
            let defaults = NSUserDefaults.standardUserDefaults()
            
            // save session
            if results.objectForKey(constants.kSession) != nil
            {
                defaults.setObject(results.objectForKey(constants.kSession), forKey: constants.kSession)
                Helper.GetAppDelegate().backend.requestHeaders.setValue(results.objectForKey(constants.kSession), forKey: constants.kSession)
            }
            
            // save token
            defaults.setObject(results.objectForKey(constants.kToken), forKey: constants.kToken)
             Helper.GetAppDelegate().backend.requestHeaders.setObject(results.objectForKey(constants.kToken)!, forKey: constants.kToken)
            
            // save user email
            if results.objectForKey(constants.kServerEmail) != nil
            {
                defaults.setObject(results.objectForKey(constants.kServerEmail), forKey: constants.kUserEmail)
            }
            
            // Show LocationPermission Screen
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! LocationPermissionScreen
            Helper.GetAppDelegate().window?.rootViewController = locationPermissionController
        }
        else
        {
            Helper.showAlertDialog(constants.kError, alertMessage:"Token not found.")
        }
    }
    
    func requestFailed(requestId: Int, errorInfo: NSDictionary)
    {
        println("requestFailed with Error \(errorInfo)")
        progressIndicatingView.hidden = true
        
        // Show alert dialog
        var errMsg = errorInfo.objectForKey(constants.kMessage) as! String
        Helper.showAlertDialog(constants.kError, alertMessage: errMsg)
    }
    
    // MARK: IBActions
    
    @IBAction func privacyAcceptButtonPressed(sender: AnyObject)
    {
        if privacyAcceptButton.selected == false
        {
            privacyAcceptButton.selected = true
        }
        else
        {
            privacyAcceptButton.selected = false
        }
    }
    
    // MARK: UITextField Delegate Method
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        container.contentSize = CGSizeMake(self.view.frame.size.width,
            submitButton.frame.size.height +
                20 + submitButton.frame.origin.y + 216);
        
        var spaceAvailable = (self.view.frame.size.height - 216)
        println(spaceAvailable)
        var desiredTextFieldYPos = spaceAvailable - textField.frame.size.height - 20
        println(desiredTextFieldYPos)
        var diff = textField.frame.origin.y - desiredTextFieldYPos;
        container.contentOffset = CGPointMake(0, diff)
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        container.contentSize = CGSizeMake(self.view.frame.size.width,
            submitButton.frame.size.height +
                20 + submitButton.frame.origin.y);
        container.contentOffset = CGPointZero
    }
}


