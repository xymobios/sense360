//
//  ConfirmationScreen.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 6/1/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import SenseSdk

class ConfirmationScreen: UIViewController, MFMailComposeViewControllerDelegate
{
    // for logging purpose
    var clickCount = Int()
    
    var constants = Constants()
    
    // Outlets
    @IBOutlet var showLogButton: UIButton!
    
    // MARK - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // if logging is enabled then only enable showLogButton
        if constants.LOG_ENABLE == 1
        {
            showLogButton.userInteractionEnabled = true
        }
        else
        {
            showLogButton.userInteractionEnabled = false
        }
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
        clickCount = 0
    }
    
    // MARK - UIActions
    
    @IBAction func showLogButtonTapped(sender: AnyObject)
    {
        clickCount = clickCount + 1
        
        // open the mail composer with Logs when button tapped three times
        if clickCount == 3
        {
            clickCount = 0
            var contents = NSString()
            let documentsDirectory : NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as! NSString
            var writableStatePath = documentsDirectory.stringByAppendingPathComponent("QPLog.txt")
            var checkValidation = NSFileManager.defaultManager()
            
            if (checkValidation.fileExistsAtPath(writableStatePath))
            {
                contents = String(contentsOfFile:writableStatePath , encoding: NSUTF8StringEncoding, error: nil)!
            }
            else
            {
                contents = "Error while reading the log file."
            }
            
            let mailComposeViewController = configuredMailComposeViewController(contents)
            if MFMailComposeViewController.canSendMail()
            {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            }
            else
            {
                self.showSendMailErrorAlert()
            }
        }
    }

    @IBAction func viewReportPressed(sender: AnyObject)
    {
        let token = Helper.token() as String?
        if token != nil && token != ""
        {
            Helper.GetAppDelegate().backend.requestHeaders.setObject(token!, forKey: constants.kToken)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let webViewController = storyboard.instantiateViewControllerWithIdentifier("webViewIdentifier") as! CustomWebView
            webViewController.urlString = constants.URL_VIEW_REPORT
            self.presentViewController(webViewController, animated: true, completion: nil)
        }
        else
        {
            Helper.showAlertDialog(constants.kError, alertMessage: "Token not found")
        }
    }
    
    // MARK - UserDefined Functions
    
    func configuredMailComposeViewController(contents: NSString) -> MFMailComposeViewController
    {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients([""])
        mailComposerVC.setSubject("LOG File")
        mailComposerVC.setMessageBody(contents as String, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert()
    {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email",
            message: "Your device could not send e-mail.  Please check e-mail configuration and try again.",
            delegate: self,
            cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK - MFMailComposeViewControllerDelegate Method
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!)
    {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}