//
//  ServerWrapper.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 6/1/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation

@objc protocol ServerWrapperDelegate
{
    optional func registrationCompleted(results:NSDictionary)
    optional func triggerInfoSentSuccessfully(results:NSDictionary)
    func requestFailed(requestId : Int, errorInfo : NSDictionary)
}

class ServerWrapper: NSObject, HTTPRequestDelegate
{
    var delegate: ServerWrapperDelegate?
    var requestHeaders = NSMutableDictionary()
    var constants = Constants()
    
    override init()
    {
        // set request Headers values
        
        let devicePlatform = Helper.devicePlatformInformation() as String
        let deviceId = Helper.deviceId() as String
        let appversion = Helper.appVersion() as String

        if !devicePlatform.isEmpty
        {
            requestHeaders.setObject(devicePlatform, forKey: constants.kPlatform)
        }
        if !deviceId.isEmpty
        {
            requestHeaders.setObject(deviceId, forKey: constants.kDeviceId)
        }
        
        if !appversion.isEmpty
        {
            requestHeaders.setObject(appversion, forKey: constants.kAppVersion)
        }
      
        let defaults = NSUserDefaults.standardUserDefaults()
        if Helper.isUserRegistred()
        {
            if let session = Helper.sessionValue() as String?
            {
                requestHeaders.setValue(session, forKey: constants.kSession)
            }
        
            let token = Helper.token() as String?
            if token != nil && token != ""
            {
                requestHeaders.setObject(token!, forKey: constants.kToken)
            }
        }
        
        requestHeaders.setObject("1.0", forKey:constants.kVersion)
        
        requestHeaders.setObject("QuantifiedPlace", forKey: constants.kAppCode)
    }

    func sendTriggerInfo(type: Int, place : NSString, action: NSString, timeStamp : Int64, delegate:ServerWrapperDelegate)
    {
        var body = "type=\(type)&place=\(place)&action=\(action)&timestamp=\(timeStamp)"

        Helper.printLog("Inside sendTriggerInfo body -> \(body)")
        
        NSLog("body %@", body)
        
        var httpBody = body.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        var urlString = constants.URL_SEND_TRIGGER_INFO as String
        var httpMethod = constants.HTTP_METHOD_POST
        var requestId = constants.REQUEST_ID_SEND_TRIGGER_INFO
        
        self.delegate = delegate
        
        if (requestHeaders.objectForKey(constants.kSession) == nil ||
        requestHeaders.objectForKey(constants.kSession)?.length == 0)
        {
            if let session = Helper.sessionValue() as String?
            {
                requestHeaders.setValue(session, forKey: constants.kSession)
            }
        }
        
        if (requestHeaders.objectForKey(constants.kToken) == nil ||
        requestHeaders.objectForKey(constants.kToken)?.length == 0)
        {
            let token = Helper.token() as String?
            if token != nil && token != ""
            {
                requestHeaders.setObject(token!, forKey: constants.kToken)
            }
        }
        
        performRequest(urlString, httpMethod: httpMethod, requestId: requestId, headers: requestHeaders, httpBody: httpBody!, delegate: self)
    }
    
    func createAccount(emailId:NSString, password: NSString, delegate:ServerWrapperDelegate)
    {
        var body = "email=\(emailId)&password=\(password)&provider=app"
        var httpBody = body.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        var urlString = Constants().URL_CREATE_ACCOUNT as String
        var httpMethod = Constants().HTTP_METHOD_POST
        var requestId = Constants().REQUEST_ID_CREATE_ACCOUNT
        self.delegate = delegate
        
        performRequest(urlString, httpMethod: httpMethod, requestId: requestId, headers: requestHeaders, httpBody: httpBody!, delegate: self)
    }
    
    func performRequest(url: NSString, httpMethod: NSString, requestId: Int, headers:NSDictionary, httpBody: NSData, delegate: AnyObject) -> HTTPRequest
    {
        var request = HTTPRequest()
        request.url = url
        request.httpMethod = httpMethod
        request.requestId = requestId
        request.postBody = httpBody
        request.delegate = self
        request.performRequest(headers)
        
        NSLog("Sending Request to %@", url)
        Helper.printLog("ServerWrapper > performRequest" + (url as String))
        return request
    }
    
    // MARK - HTTPRequest Delegate Methods
    
    func dataReceived(owner: HTTPRequest, results: NSDictionary)
    {
        Helper.printLog("ServerWrapper > dataReceived - \(results)")
        if results.objectForKey(constants.kServerStatus)?.integerValue == 200
        {
            if owner.requestId == constants.REQUEST_ID_CREATE_ACCOUNT
            {
                if let delegate = self.delegate
                {
                    delegate.registrationCompleted!(results)
                }
            }
            else if owner.requestId == constants.REQUEST_ID_SEND_TRIGGER_INFO
            {
                if let delegate = self.delegate
                {
                    delegate.triggerInfoSentSuccessfully!(results)
                }
            }
        }
        else
        {
            var errorMessage = Constants().kNetworkFailed
            
            if results.objectForKey(constants.kServerError) != nil &&
                results.objectForKey(constants.kServerError)?.objectForKey(constants.kServerMsg) != nil
            {
                errorMessage = results.objectForKey(constants.kServerError)?.objectForKey(constants.kServerMsg) as! String
            }

            var code = results.objectForKey(constants.kServerError)?.objectForKey(constants.kCode) as! Int
            var errorDict : NSDictionary = [constants.kMessage : errorMessage, constants.kCode : code]
            requestFailed(owner, results: errorDict)
        }
    }
    
    func requestFailed(owner: HTTPRequest, results: NSDictionary)
    {
        Helper.printLog("ServerWrapper > requestFailed - \(results) ")
        if let delegate = self.delegate
        {
            delegate.requestFailed(owner.requestId!, errorInfo: results)
        }
    }
}


